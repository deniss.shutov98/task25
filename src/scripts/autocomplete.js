import valuesArray from "../constants/current.city.list.json";

export default function autocomplete(textFieldElement) {
  textFieldElement.addEventListener("input", function () {
    let currentTextFieldValue = this.value;
    closeAllLists();
    if (!currentTextFieldValue) {
      return false;
    }

    let autocompleteList = document.createElement("DIV");
    autocompleteList.setAttribute("id", `${this.id}autocomplete-list`);
    autocompleteList.classList.add("autocomplete-items");
    this.parentNode.appendChild(autocompleteList);

    for (let value of valuesArray) {
      let city = value.name;
      if (
        city.substr(0, currentTextFieldValue.length).toUpperCase() ==
        currentTextFieldValue.toUpperCase()
      ) {
        autocompleteList.appendChild(
          createListElement(city, currentTextFieldValue)
        );
      }
    }
  });

  function createListElement(content, currentTextFieldValue) {
    let newElement = document.createElement("DIV");
    newElement.innerHTML = `<strong>${content.substr(
      0,
      currentTextFieldValue.length
    )}</strong>`;
    newElement.innerHTML += content.substr(currentTextFieldValue.length);
    newElement.innerHTML += `<input type='hidden' value='${content}'>`;
    newElement.classList.add("autocomplete-item-container");
    newElement.addEventListener("click", function () {
      textFieldElement.value = this.getElementsByTagName("input")[0].value;
      closeAllLists();
    });
    return newElement;
  }

  document.addEventListener("click", function (mouseEvent) {
    closeAllLists(mouseEvent.target);
  });

  function closeAllLists(activeElement) {
    let autocomleteItemsElements =
      document.getElementsByClassName("autocomplete-items");
    for (let element of autocomleteItemsElements) {
      if (activeElement != element && activeElement != textFieldElement) {
        element.parentNode.removeChild(element);
      }
    }
  }
}
