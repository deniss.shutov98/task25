import valuesArray from "../constants/current.city.list.json";

export default async function fetchWeatherData(city) {
  let key = "2f73963a7928497bacd31de5ca1db0af";
  let record = getRecord(city);
  let promise = fetch(
    `https://api.openweathermap.org/data/2.5/onecall?lat=${record.coord.lat}&lon=${record.coord.lon}&appid=${key}`
  );
  return promise
    .then((response) => response.json())
    .then((weatherData) => {
      return weatherData;
    });
}

function getRecord(cityName) {
  for (let value of valuesArray) {
    if (value.name === cityName) {
      return value;
    }
  }
}
