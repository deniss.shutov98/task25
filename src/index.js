import "./styles/styles.css";
import fetchWeatherData from "./scripts/fetch.js";
import autocomplete from "./scripts/autocomplete.js";

const form = document.getElementById("cityInputForm");
const textFieldElement = document.getElementById("cityInput");
const selectedLabel = document.getElementById("selectedCity");
const weatherTable = document.getElementById("ForecastTable");

autocomplete(textFieldElement);
textFieldElement.value = "London";
reloadWeather();

form.addEventListener("submit", async (event) => {
  event.preventDefault();
  await reloadWeather();
});

async function reloadWeather() {
  selectedLabel.innerHTML = `Selected city: ${textFieldElement.value}`;
  let data = await fetchWeatherData(textFieldElement.value);
  for (let i = 1; i <= data.daily.length; i++) {
    if (weatherTable.rows[i] != undefined) {
      weatherTable.deleteRow(i);
    }
    addWeatherRow(data.daily[i], i);
  }
}

function addWeatherRow(data, rowIndex) {
  if (data == undefined) {
    return;
  }
  let row = weatherTable.insertRow(rowIndex);
  row.classList.add("data-row");

  let dataCell = row.insertCell(0);
  let weatherCell = row.insertCell(1);
  let DayTempCell = row.insertCell(2);
  let NightTempCell = row.insertCell(3);

  dataCell.classList.add("weather-table-cell");
  weatherCell.classList.add("weather-table-cell");
  DayTempCell.classList.add("weather-table-cell");
  NightTempCell.classList.add("weather-table-cell");

  dataCell.innerHTML = timeConverter(data.dt);
  weatherCell.innerHTML = `<img src='https://openweathermap.org/img/wn/${data.weather[0].icon}@2x.png' 
          title='${data.weather[0].description}' width='50' height='50'/>`;
  DayTempCell.innerHTML = `${getCelcius(data.temp.day)}&deg;C`;
  NightTempCell.innerHTML = `${getCelcius(data.temp.night)}&deg;C`;
}

function getCelcius(temp) {
  return Math.round(parseFloat(temp) - 273.15);
}

function timeConverter(UNIX_timestamp) {
  let date = new Date(UNIX_timestamp * 1000);
  const months = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  let month = months[date.getMonth()];
  let day = date.getDate();
  return `${day} ${month}`;
}
